from django.db import models
from datetime import datetime, date

class Topik(models.Model):
    nama = models.CharField(max_length=50)

    def __str__(self):
        return self.nama

class Komentar(models.Model):
    nama = models.CharField(max_length=10)
    komentar = models.TextField(max_length=100)
    waktu = models.DateTimeField(auto_now_add=True)
    topik = models.ForeignKey(Topik, on_delete=models.CASCADE, null=True)
    
    def __str__(self):
        return self.nama