from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Komentar, Topik
from .views import welcome, topik, forum

# Create your tests here.

# URL TEST
class TestStory6(TestCase):
    def setUp(self):
        self.bahasan = Topik.objects.create(
            nama = 'tester topik',
            id=1
        )
        self.komen = Komentar.objects.create(
            nama = 'budi',
            komentar = 'ini bapak budi',
            topik = self.bahasan,
            id=1
        )


# Test URL
    def test_apakah_url_chitchat_ada(self):
        client = Client()
        response = client.get('/chitchat/')
        self.assertEquals(response.status_code, 200)

    def test_apakah_url_topik_ada(self):
        client = Client()
        response = client.get('/chitchat/topik')
        self.assertEquals(response.status_code, 200)
        
    def test_apakah_url_forum_ada(self):
        client = Client()
        response = client.get('/chitchat/forum/1/budi')
        self.assertEquals(response.status_code, 200)

# Test View   
    def test_view_welcome(self):
        url = '/chitchat/'
        response = Client().get(url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'welcome.html')

    def test_view_topik(self):
        url = '/chitchat/topik'
        response = Client().get(url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'topik.html')

    def test_view_forum(self):
        url = '/chitchat/forum/1/budi'
        response = Client().get(url)
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'forum.html')

#Model test
    def test_Topik(self):
        testTopik = Topik.objects.get(id=1)
        self.assertEqual(testTopik.nama, "tester topik")
        self.assertEqual(str(testTopik), "tester topik")

    def test_peserta(self):
        testkomentar = Komentar.objects.get(id=1)
        self.assertEqual(str(testkomentar), "budi")
        self.assertEqual(testkomentar.nama,"budi")
        self.assertEqual(testkomentar.komentar,"ini bapak budi")
        self.assertEqual(testkomentar.topik, Topik.objects.get(id=1))
