from django.contrib import admin
from .models import Komentar, Topik

admin.site.register(Topik)
admin.site.register(Komentar)
