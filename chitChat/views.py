from django.shortcuts import render
from .models import Topik, Komentar
from .forms import FormKomentar

def welcome(request):
    return render(request, 'welcome.html', {'title': 'Chitchat'})

def topik(request):
    nama = request.GET.get('nama', False)
    alltopik = Topik.objects.all()
    responses = {
        'nama': nama,
        'title': 'Chitchat - Topik',
        'alltopik' : alltopik,
    }
    return render(request, 'topik.html', responses)

def topik2(request,name):
    alltopik = Topik.objects.all()
    responses = {
        'nama': name,
        'title': 'Chitchat - Topik',
        'alltopik' : alltopik,
    }
    return render(request, 'topik.html', responses)

def forum(request,id,name):
    topik = Topik.objects.get(id=id)
    komentar = Komentar.objects.filter(topik__nama = topik.nama)
    
    if request.POST:
        form = FormKomentar(request.POST)
        if form.is_valid():
            form.save()

    form = FormKomentar(initial= {'topik': id, 'nama': name,})
    responses = {
        'komentar': komentar,
        'title': 'Chitchat - Forum',
        'topik' : topik,
        'nama' : name,
        'form' : form,
    }
    return render(request, 'forum.html', responses)
    

