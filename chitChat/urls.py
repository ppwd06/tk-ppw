from django.urls import include, path
from chitChat import views

urlpatterns = [
	path('', views.welcome, name='welcome'),
	path('topik/', views.topik, name='topik'),
	path('forum/', views.forum, name='forum'),
	path('topik', views.topik, name='topik'),
	path('topik2/<str:name>', views.topik2, name='topik2'),
	path('forum/<int:id>/<str:name>', views.forum, name='forum'),
]