from django.contrib import admin
from django.urls import path
# from LearnAJAX import views as a
# from Quiz import views
from .views import welcom, quiz, result, show
from django.conf import settings
from django.conf.urls.static import static

app_name = 'quiz'


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', welcom, name='welcom'),
    path('quiz/', quiz, name='quiz'),
    path('result/', result, name='result'),
    path('showAnswer/', show, name='show'),
    ]