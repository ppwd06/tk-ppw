from django.test import TestCase
from django.test import Client 
from django.urls import resolve, reverse
from .models import Questions, Answer
from .forms import FormQuestion
from .views import welcom, quiz, result, show
from django.apps import apps
from .apps import QuizConfig
# Create your tests here.

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(QuizConfig.name, 'quiz') 
        self.assertEqual(apps.get_app_config('quiz').name, 'quiz')

class ModelTest(TestCase):
    def setUp(self):
        self.question = Questions.objects.create(
            question="what?", option1 ="a", option2="b", option3="c", option4="d", answer="a")
        self.answer = Answer.objects.create(
            nama="Ferika", 
            result="1", 
            ans1 = "a",
            ans2 = "a",
            ans3 = "a",
            ans4 = "a",
            ans5 = "a",
            ans6 = "a",
            ans7 = "a",
            ans8 = "a",
            ans9 = "a",
            ans10 = "a",
            )

    def test_model_dibuat(self):
        self.assertEqual(Questions.objects.count(), 1)
        self.assertEqual(Answer.objects.count(), 1)

    def test_str(self):
        self.assertEquals(str(self.question), "what?")
        self.assertEquals(str(self.answer), "Ferika")

class FormTest(TestCase):
    def test_form_is_valid(self):
        form_question = FormQuestion(data={
            "nama": "Ferika",
            "question1" : "Mahkota",
            "question2" : "Mahkota",
            "question3" : "Mahkota",
            "question4" : "Mahkota",
            "question5" : "Mahkota",
            "question6" : "Mahkota",
            "question7" : "Mahkota",
            "question8" : "Mahkota",
            "question9" : "Mahkota",
            "question10" : "Mahkota",
        })
        self.assertTrue(form_question.is_valid())

class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_welcome_ada(self):
        response = Client().get('/quiz')
        self.assertEquals(response.status_code, 301)
    
    def test_url_kuis_ada(self):
        response = Client().get('/quiz/quiz')
        self.assertEquals(response.status_code, 301)
    
    def test_url_result_ada(self):
        response = Client().get('/quiz/result')
        self.assertEquals(response.status_code, 301)
    
    def test_url_show_ada(self):
        response = Client().get('/quiz/showAnswer')
        self.assertEquals(response.status_code, 301)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.welcom = reverse("quiz:welcom")
        self.quiz = reverse("quiz:quiz")
        self.result = reverse("quiz:result")
        self.show = reverse("quiz:show")

    def test_GET_welcom(self):
        response = self.client.get(self.welcom)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'quiz/welcom.html')

    def test_GET_quiz(self):
        response = self.client.get(self.quiz, {'question': 'what?', 'option1':'a', 'option2':'b', 'option3':'c', 'option4':'d', 'answer':'a'}, follow = True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'quiz/quiz.html')
        
    def test_views_show(self):
        response = self.client.get(self.show)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'quiz/show.html')

    def test_POST_quiz(self):
        response = self.client.post(self.quiz,
        {
            "nama": "Ferika",
            "question1" : "Mahkota",
            "question2" : "Mahkota",
            "question3" : "Mahkota",
            "question4" : "Mahkota",
            "question5" : "Mahkota",
            "question6" : "Mahkota",
            "question7" : "Mahkota",
            "question8" : "Mahkota",
            "question9" : "Mahkota",
            "question10" : "Mahkota",
        }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_scor_is_update(self):
        response = self.client.post(self.quiz,
        {
            "nama": "Ferika",
            "question1" : "mahkota",
            "question2" : "d",
            "question3" : "b",
            "question4" : "c",
            "question5" : "d",
            "question6" : "a",
            "question7" : "b",
            "question8" : "c",
            "question9" : "d",
            "question10" : "c",
        }, follow=True)
        self.assertEqual(response.status_code, 200)