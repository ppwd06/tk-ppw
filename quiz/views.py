from django.shortcuts import render, redirect
from .models import Questions, Answer
from .forms import FormQuestion
from django.http import HttpResponseRedirect
# from django.core.paginator import Paginator
# Create your views here.



def welcom(request):
    return render(request, 'quiz/welcom.html')

def quiz(request):
    form_question = FormQuestion(request.POST or None)
    if (form_question.is_valid() and request.method == 'POST'):
        cd = form_question.cleaned_data
        Answer.objects.create(
            nama = cd['nama'],
            ans1 = cd['question1'],
            ans2 = cd['question2'],
            ans3 = cd['question3'],
            ans4 = cd['question4'],
            ans5 = cd['question5'],
            ans6 = cd['question6'],
            ans7 = cd['question7'],
            ans8 = cd['question8'],
            ans9 = cd['question9'],
            ans10 = cd['question10'],
        )
        return redirect('quiz:result')
    context = {
        'form_question':form_question,
    }


    return render(request, 'quiz/quiz.html', context)

def result(request):
    score = 0
    ques = Answer.objects.latest('id')

    if ques.ans1 == "mahkota" :
        score += 1
    if ques.ans2 == 'd' :
        score += 1
    if ques.ans3 == 'b' :
        score += 1
    if ques.ans4 == 'c' :
        score += 1
    if ques.ans5 == 'd' :
        score += 1
    if ques.ans6 == 'a' :
        score += 1
    if ques.ans7 == 'b' :
        score += 1
    if ques.ans8 == 'c' :
        score += 1
    if ques.ans9 == 'd' :
        score += 1
    if ques.ans10 == 'c' :
        score += 1


    ques.result = score
    ques.save()
    orang = Answer.objects.all()
    context = {
        'orang':orang,
        'orangbaru' : ques,
    }

    return render(request, 'quiz/results.html', context)

def show(request):
    ques = Questions.objects.all()
    return render(request, 'quiz/show.html', {'ques':ques})
