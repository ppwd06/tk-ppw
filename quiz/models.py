from django.db import models

# Create your models here.

class Questions(models.Model):
    question = models.CharField(max_length=300, null=True)
    option1 = models.CharField(max_length=200)
    option2 = models.CharField(max_length=200)
    option3 = models.CharField(max_length=200)
    option4 = models.CharField(max_length=200)
    answer = models.CharField(max_length=200)

    def __str__(self):
        return self.question

class Answer(models.Model):
    nama = models.CharField(max_length=100)
    result = models.IntegerField(null=True)
    ans1 = models.CharField(max_length=200, null=True)
    ans2 = models.CharField(max_length=200, null=True)
    ans3 = models.CharField(max_length=200, null=True)
    ans4 = models.CharField(max_length=200, null=True)
    ans5 = models.CharField(max_length=200, null=True)
    ans6 = models.CharField(max_length=200, null=True)
    ans7 = models.CharField(max_length=200, null=True)
    ans8 = models.CharField(max_length=200, null=True)
    ans9 = models.CharField(max_length=200, null=True)
    ans10 = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.nama

