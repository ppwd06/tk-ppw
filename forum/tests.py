from django.test import TestCase, Client
from django.urls import resolve
from .views import forum
from .models import Pesan
from .forms import FormPesan

class ForumTest(TestCase):
    
    def test_forum_url_is_exist(self):
        response = Client().get('/forum/')
        self.assertEqual(response.status_code, 200)
    
    def test_forum_using_forum_function(self):
        found = resolve('/forum/')
        self.assertEqual(found.func, forum)
    
    def test_forum_using_template(self):
        response = Client().get('/forum/')
        self.assertTemplateUsed(response, 'post.html')
        
class ModelTest(TestCase):
    
    def test_object_to_string_method(self):
        post1 = Pesan.objects.create(nama='user', pesan='Lorem ipsum')
        post2 = Pesan.objects.create(nama='test', pesan='Lorem ipsum dolor sit amet, consectetur adipiscing elit')
        self.assertEqual(str(post1), 'user: Lorem ipsum')
        self.assertEqual(str(post2), 'test: Lorem ipsum dolor sit...')
        
    def test_form_creates_new_object(self):
        form_data = {'nama':'test', 'pesan':'Lorem ipsum'}
        response = Client().post('/forum/', data=form_data)
        self.assertEqual(Pesan.objects.count(), 1)