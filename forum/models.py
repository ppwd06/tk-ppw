from django.db import models

class Pesan(models.Model):

    nama = models.CharField(max_length=20)
    pesan = models.TextField()
    waktu = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        if len(self.pesan) <= 20:
            return "{}: {}".format(self.nama, self.pesan)
        else:
            return "{}: {}...".format(self.nama, self.pesan[0:21])
