from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import FormPesan
from .models import Pesan

def forum(request):
    if (request.method == 'POST'):
        form = FormPesan(request.POST or None)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/forum')
    else:
        form = FormPesan()
        post = Pesan.objects.all().order_by('-id')
        jumlah = Pesan.objects.count()
        context = { 'form':form, 'post':post, 'jumlah':jumlah, 'title':'Forum Sharing' }
        return render(request, 'post.html', context)
