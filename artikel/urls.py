from django.urls import path
from . import views

app_name = 'artikel'

urlpatterns = [
    path('', views.index, name='index'),
    path('buat-artikel', views.forms, name='forms'),
    path('detail/<slug:slug>', views.detail, name='detail'),
    path('category/<str:category>', views.category, name='category'),
    path('data/', views.data, name='data'),
    path('index2/', views.index2, name='index2'),
    path('data-detail/', views.data_detail, name='data_detail'),
    path('detail2/', views.detail2, name='detail2'),
    path('data-search/', views.data_search, name='data_search'),
    path('search/', views.search, name='search'),
]
