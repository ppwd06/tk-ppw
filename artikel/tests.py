from django.test import TestCase
from django.urls import reverse
from .models import Artikel

# Create your tests here.


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/artikel/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'artikel/index.html')
        response = self.client.get(reverse('artikel:index'))
        self.assertEqual(response.status_code, 200)

    def test_can_create_a_kegiatan(self):
        artikel1 = Artikel.objects.create(
            title="Data Science Keren", content='Keren Banget Parah', author='Uzumaki Naruto', category='Teknologi')
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 1)

        artikel_data_science = Artikel.objects.get(id=1)
        self.assertEqual(str(artikel_data_science),
                         'Data Science Keren - Uzumaki Naruto')

    def test_can_save_a_POST_artikel_request(self):
        response = self.client.post(
            '/artikel/buat-artikel', data={'title': "Data Science Keren", 'content': 'Keren Banget Parah', 'author': 'Uzumaki Naruto', 'category': 'Teknologi'})
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/artikel/')

        new_response = self.client.get('/artikel/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Data Science Keren', html_response)

    def test_forms_url_status_200(self):
        response = self.client.get('/artikel/buat-artikel')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'artikel/forms.html')
        response = self.client.get(reverse('artikel:forms'))
        self.assertEqual(response.status_code, 200)

    def test_detail_url(self):
        artikel1 = Artikel.objects.create(
            title="Data Science Keren", content='Keren Banget Parah', author='Uzumaki Naruto', category='Teknologi')
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 1)

        new_response = self.client.get('/artikel/detail/' + artikel1.slug)
        html_response = new_response.content.decode('utf8')
        self.assertIn('Keren Banget Parah', html_response)
        self.assertTemplateUsed(new_response, 'artikel/detail.html')

    def test_category_url(self):
        artikel1 = Artikel.objects.create(
            title="Data Science Keren", content='Keren Banget Parah', author='Uzumaki Naruto', category='Teknologi')
        artikel2 = Artikel.objects.create(
            title="Data Science Academy Jadi Tiga Minggu", content='InsyaAllah bermanfaat dan seru', author='Uchiha Sasuke', category='Berita')
        counting_all_available_artikel = Artikel.objects.all().count()
        self.assertEqual(counting_all_available_artikel, 2)

        new_response = self.client.get(
            '/artikel/category/' + artikel2.category)
        html_response = new_response.content.decode('utf8')
        self.assertIn('Data Science Academy Jadi Tiga Minggu', html_response)
        self.assertTemplateUsed(new_response, 'artikel/category.html')
