from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import JsonResponse

import json
import urllib.request

from .forms import ArtikelForm
from .models import Artikel


def index(request):
    list_artikel = Artikel.objects.all()[::-1]
    page = request.GET.get('page', 1)

    paginator = Paginator(list_artikel, 5)
    try:
        artikel = paginator.page(page)
    except PageNotAnInteger:
        artikel = paginator.page(1)
    except EmptyPage:
        artikel = paginator.page(paginator.num_pages)

    context = {
        'title': 'Artikel',
        'list_artikel': artikel
    }

    return render(request, 'artikel/index.html', context)


def forms(request):
    artikel_form = ArtikelForm(request.POST or None)
    if request.method == 'POST':
        if artikel_form.is_valid():
            artikel_form.save()

            return redirect('artikel:index')

    context = {
        'title': 'Buat Artikel',
        'artikel_form': artikel_form
    }

    return render(request, 'artikel/forms.html', context)


def detail(request, slug):
    artikel = Artikel.objects.get(slug=slug)

    context = {
        'title': artikel.title,
        'artikel': artikel
    }

    return render(request, 'artikel/detail.html', context)


def category(request, category):
    list_artikel = Artikel.objects.filter(category=category)[::-1]
    page = request.GET.get('page', 1)

    paginator = Paginator(list_artikel, 5)
    try:
        artikel = paginator.page(page)
    except PageNotAnInteger:
        artikel = paginator.page(1)
    except EmptyPage:
        artikel = paginator.page(paginator.num_pages)

    context = {
        'title': category,
        'list_artikel': artikel
    }

    return render(request, 'artikel/category.html', context)


def data(request):

    url_news = "https://www.news.developeridn.com/"
    connection = urllib.request.urlopen(url_news)
    js = connection.read()
    info = json.loads(js.decode("utf-8"))

    return JsonResponse(info)


def index2(request):

    context = {
        'title': 'Artikel',
    }

    return render(request, 'artikel/index2.html', context)


def data_detail(request):

    url = request.GET.get(
        'url', 'https://www.cnnindonesia.com/internasional/20200513095240-134-502769/turis-jatuh-usai-nekat-kunjungi-yellowstone-saat-pandemi')

    url_news = "https://www.news.developeridn.com/detail/?url="
    connection = urllib.request.urlopen(url_news + url)
    js = connection.read()
    info = json.loads(js.decode("utf-8"))

    return JsonResponse(info)


def detail2(request):

    url = request.GET.get(
        'url', 'https://www.cnnindonesia.com/internasional/20200513095240-134-502769/turis-jatuh-usai-nekat-kunjungi-yellowstone-saat-pandemi')

    context = {
        'title': 'Artikel',
        'url': url
    }

    return render(request, 'artikel/detail2.html', context)


def data_search(request):

    q = request.GET.get(
        'q', 'indonesia')

    url_news = "https://www.news.developeridn.com/search/?q="
    connection = urllib.request.urlopen(url_news + q)
    js = connection.read()
    info = json.loads(js.decode("utf-8"))

    return JsonResponse(info)


def search(request):

    q = request.GET.get(
        'q', 'indonesia')

    q = 'indonesia' if q == '' else q

    context = {
        'title': q,
        'q': q
    }

    return render(request, 'artikel/index2.html', context)
