from django.db import models
from django.utils import timezone
from django.utils.text import slugify

# Create your models here.


class Artikel(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    author = models.CharField(max_length=25)

    LIST_CATEGORY = (
        ('Berita', 'Berita'),
        ('Teknologi', 'Teknologi'),
        ('Hobi', 'Hobi'),
        ('Kesehatan', 'Kesehatan'),
    )

    category = models.CharField(
        max_length=10, choices=LIST_CATEGORY, default='Berita')

    date_posted = models.DateTimeField(default=timezone.now)
    slug = models.CharField(max_length=150, editable=False)

    def save(self, *args, **kwargs):
        self.slug = slugify(
            "{}-{}".format(self.title, self.author))
        super(Artikel, self).save()

    def __str__(self):
        return "{} - {}".format(self.title, self.author)
