from django import forms

from .models import Artikel


class ArtikelForm(forms.ModelForm):

    class Meta:
        model = Artikel
        fields = [
            'title',
            'content',
            'author',
            'category',
        ]
        labels = {
            'title': 'Judul :',
            'content': 'Konten :',
            'author': 'Penulis :',
            'category': 'Kategori :',
        }
        widgets = {
            'title': forms.TextInput(
                attrs={
                    'class': 'form-control content-section',
                    'placeholder': 'Judul Artikel'
                }
            ),
            'content': forms.Textarea(
                attrs={
                    'class': 'form-control content-section',
                    'placeholder': 'Konten Artikel'
                }
            ),
            'author': forms.TextInput(
                attrs={
                    'class': 'form-control content-section',
                    'placeholder': 'Penulis Artikel'
                }
            ),
            'category': forms.Select(
                attrs={
                    'class': 'form-control content-section'
                }
            ),
        }
