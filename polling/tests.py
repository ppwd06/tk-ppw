from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import PollModel
from django.apps import apps
from .views import index, poll, listpoll, result
from .forms import PollForm

# Create your tests here.
class TestModel(TestCase):
    def setUp(self):
        self.poll = PollModel.objects.create(activity="olahraga")

    def test_instance_created(self):
        self.assertEqual(PollModel.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.poll), "olahraga")

class TestForm(TestCase):

    def test_form_is_valid(self):
        poll_form = PollForm(data={
            "activity": "olahraga"
        })
        self.assertTrue(poll_form.is_valid())
        

    def test_form_invalid(self):
        poll_form = PollForm(data={})
        self.assertFalse(poll_form.is_valid())

class TestUrls(TestCase):

    def setUp(self):
        self.activity = PollModel.objects.create(activity="olahraga")
        self.index = reverse("polling:index")
        self.listpoll = reverse("polling:listpoll")
        self.poll = reverse("polling:poll")
        self.result = reverse("polling:result")

    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)
    
    def test_listpoll_use_right_function(self):
        found = resolve(self.listpoll)
        self.assertEqual(found.func, poll)

    def test_poll_use_right_function(self):
        found = resolve(self.poll)
        self.assertEqual(found.func, poll)

    def test_result_use_right_function(self):
        found = resolve(self.result)
        self.assertEqual(found.func, result)
 
class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index = reverse("polling:index")
        self.listpoll = reverse("polling:listpoll")
        self.poll = reverse("polling:poll")
        self.result = reverse("polling:result")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'polling/index.html')

    def test_GET_listpoll(self):
        response = self.client.get(self.listpoll)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'polling/poll.html')
    
    def test_GET_result(self):
        response = self.client.get(self.result)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'polling/result.html')

    def test_GET_poll(self):
        response = self.client.get(self.poll)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'polling/poll.html')
# 
    def test_POST_poll(self):
        response = self.client.post(self.poll,
                                    {
                                        'activity': "olahraga"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_poll_invalid(self):
        response = self.client.post(self.poll,
                                    {
                                        'activity': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'polling/poll.html')

  