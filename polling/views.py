from django.shortcuts import render, redirect
from django.http import HttpResponse

from .forms import PollForm
from .models import PollModel

# Create your views here.
def index(request):
    context = {
        'title' : 'Poll'
    }
    return render(request, 'polling/index.html', context)

def listpoll(request):
    context = {
        'title' : 'Poll',
        'form':PollForm
    }
    return render(request, 'polling/poll.html', context)

def poll(request):
    form = PollForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        cd = form.cleaned_data
        PollModel.objects.create(activity = cd['activity'])
        # form.save()
        return redirect('polling:result')
    context = {
        'title' : 'Poll',
        'form': form
    }
    return render(request, 'polling/poll.html', context)


def result(request):
    option1 = PollModel.objects.filter(activity='olahraga').count()
    option2 = PollModel.objects.filter(activity='bacabuku').count()
    option3 = PollModel.objects.filter(activity='bingewatch').count()
    option4 = PollModel.objects.filter(activity='kuliah').count()

    context = {
        'title' : 'Poll',
        'option1': option1,
        'option2' : option2,
        'option3' : option3,
        'option4' : option4
    }
    return render(request, 'polling/result.html', context)

# 
    
