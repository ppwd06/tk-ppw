# Generated by Django 3.1.3 on 2020-11-14 09:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polling', '0002_auto_20201114_0850'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pollmodel',
            name='option_four_count',
        ),
        migrations.RemoveField(
            model_name='pollmodel',
            name='option_one_count',
        ),
        migrations.RemoveField(
            model_name='pollmodel',
            name='option_three_count',
        ),
        migrations.RemoveField(
            model_name='pollmodel',
            name='option_two_count',
        ),
    ]
