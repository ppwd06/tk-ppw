from django.urls import include, path
from . import views

app_name = 'polling'

urlpatterns = [
    path('poll-result/', views.result, name='result'),
    path('poll/', views.poll, name='poll'),
    path('listpoll/', views.poll, name='listpoll'),
	path('', views.index, name='index'),
]